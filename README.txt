This module demonstrates how to create a filtered, tabular list with EntityLister.

Copy the following into your theme:

* entity_lister/entity-lister-list--tabular.tpl.php
* entity_lister_example/user-profile--entity-lister-example-table.tpl.php
